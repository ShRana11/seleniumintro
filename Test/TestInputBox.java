import static org.junit.Assert.*;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class TestInputBox {
	WebDriver driver;
	final String URL = "https://www.seleniumeasy.com/test/basic-first-form-demo.html";
	final String DriverPath = "/Users/sukhrana/Desktop/chromedriver";

	@Before
	public void setUp() throws Exception {
		System.setProperty("webdriver.chrome.driver",DriverPath);
		driver = new ChromeDriver();
		driver.get(URL);
		
	}

	@After
	public void tearDown() throws Exception {
		TimeUnit.SECONDS.sleep(5);  // or Thread.sleep(3000)
		driver.close();
	}

	@Test
	public void testSingleInputField() throws InterruptedException {
		
		//1. find text box (id = user-message)
		
		//2. type into text box (.sendKeys("..."))
		//3. find the button (form#get-input button)
		//4. click on the button(.click())
		//5. find output message thing (id = display)
		//6. check the output message(.getText())
		WebElement textBox = driver.findElement(By.id("user-message"));
		textBox.sendKeys("hello world");
		WebElement button = driver.findElement(By.cssSelector("form#get-input button"));
		button.click();
		WebElement outputSpan = driver.findElement(By.id("display"));
		
		String outputmessage = outputSpan.getText();
		assertEquals("hello world", outputmessage);
		

	}
	@Test
	public void testTwoInputField() throws InterruptedException {
		
		
		WebElement textBox1 = driver.findElement(By.id("sum1"));
		textBox1.sendKeys("10");
		WebElement textBox2 = driver.findElement(By.id("sum2"));
		textBox2.sendKeys("10");
		WebElement button = driver.findElement(By.cssSelector("form#gettotal button"));
		button.click();
		WebElement outputSpan = driver.findElement(By.id("displayvalue"));
		String outputmessage = outputSpan.getText();
		
		assertEquals("100", outputmessage);
		
		
		
	}

}
