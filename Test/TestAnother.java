import static org.junit.Assert.*;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class TestAnother {

	WebDriver driver;
	final String URL = "https://www.seleniumeasy.com/test/basic-checkbox-demo.html";
	final String DriverPath = "/Users/sukhrana/Desktop/chromedriver";

	@Before
	public void setUp() throws Exception {
		System.setProperty("webdriver.chrome.driver",DriverPath);
		driver = new ChromeDriver();
		driver.get(URL);
		
	}

	@After
	public void tearDown() throws Exception {
		TimeUnit.SECONDS.sleep(2);  // or Thread.sleep(3000)
		driver.close();
	}
	@Test
	public void testSingleCheckBox() {
		WebElement CheckBox = driver.findElement(By.id("isAgeSelected"));
		CheckBox.click();
		boolean isChecked = CheckBox.isSelected();
		assertTrue(isChecked);
		WebElement outputD = driver.findElement(By.id("txtAge"));
		String actual = outputD.getText();
		String Expected = "Success - Check box is checked";
		assertEquals(actual, Expected);
	}


	@Test
	public void testMulCheckBox() {
		/*
		 * Press button
		 * -> all boxes get checked
		 * -> word of button change to uncheck all
		 * 
		 * Again Press
		 * -> checkbox not selected
		 * -> word changes to check
		 * 
		 */
		
		WebElement button = driver.findElement(By.id("check1"));
		button.click();
		String buttonText = button.getAttribute("value");
		assertEquals("Uncheck All", buttonText);
		List<WebElement> CheckButton =  driver.findElements(By.cssSelector("label input.cb1-element"));
//		boolean isChecked = CheckButton.isSelected();
//		assertTrue(isChecked);
		
		
		
	}

}
